# Back to the Basics - Update aplikacji z K8s: App

To zwykła strona z napisem na kolorowym tle...

## Po co to?

Jest to strona napisana na potrzeby prezentacji na Cloud Native & Kubernetes Meetup Białystok #3.
Powstała tylko po to aby na jej podstawie omówić różne strategie aktualizacji aplikacji opartej na kubernetes.

### Budowanie obrazu

Do zbudowania obrazu używamy po prostu:

```
docker build -t onrec/meetup3:v1
```
